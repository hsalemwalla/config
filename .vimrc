" Hussein's VIMRC

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin management
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Install junegunn/vim-plug for package management if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim plugins
call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'altercation/vim-colors-solarized'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'majutsushi/tagbar'
Plug 'myusuf3/numbers.vim'
call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic configuration settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Break some bad habits - nuke the arrow keys
noremap <LEFT>  <NOP>
noremap <RIGHT> <NOP>
noremap <UP>    <NOP>
noremap <DOWN>  <NOP>

" Get rid of vi compatibility as its limiting
set nocompatible

" No need for select mode, visual mode is enough
set selectmode=

" Highlight search terms
set hlsearch

" Make backspace act like it should
set whichwrap+=<,>,h,l

" Height of the command bar
set cmdheight=2

" Ignore case when searching unless search has capitalization
set smartcase

" Use spaces instead of tabs and be smart with them
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Make indenting smart too
set autoindent
set smartindent

" See :help :behave" to illustrate what this command does
" tl;dr: Makes vim more UNIX like vs mswin
behave xterm

" Turn off backup, as everything is in VCS anyways
set nobackup
set nowritebackup

" Change to the directory of the file selected 
set autochdir 

" Colors and colorscheme
colorscheme solarized


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Custom key bindings and functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Open this file automatically
nnoremap <leader><c-v> :e $HOME/.vimrc<CR>

" Make it easier to navigate long lines
map j gj
map k gk

" Disable highlighting with <leader><cr>
map <silent> <leader><CR> :noh<CR>

" Delete trailing whitespace - run on python files
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()

" Vertical split the current buffer
nnoremap <leader>s :vsp<CR>

" Return to last edit position when opening files (You want this!) 
autocmd BufReadPost *    
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \ exe "normal! g`\"" |
            \ endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin specific configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" vim-easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)


" tagbar
let g:tagbar_left=1 
nnoremap <leader>to :TagbarToggle<CR>
